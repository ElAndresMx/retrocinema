package com.anadromo.anddresmx.cinefit.Interfaces;

import com.anadromo.anddresmx.cinefit.Models.TransactionRequest;
import com.anadromo.anddresmx.cinefit.Models.UserTransactionResponse;
import com.anadromo.anddresmx.cinefit.Models.UserResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by amartineza on 3/23/2018.
 */

public interface SporaServices {

    @Headers({
            "api_key:cinepolis_test_android",
            "Content-Type:application/json"
    })
    @POST("v1/members/loyalty")
    Call<UserTransactionResponse> getMembersLoyalty(@Header("Authorization") String accessToken , @Body TransactionRequest bodyMembersLoyalty);

    @Headers({
            "api_key:cinepolis_test_android",
            "Content-Type:application/x-www-form-urlencoded"
    })
    @FormUrlEncoded
    @POST("v2/oauth/token")
    Call<UserResponse> loginAttempt(
            @Field("country_code") String countryCode,
            @Field("username") String username,
            @Field("password") String password,
            @Field("grant_type") String grantType,
            @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret
    );




    /*@GET("v1/cities")
    Call<List<CitiesModel>> getCities(); */
}
