package com.anadromo.anddresmx.cinefit.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.anadromo.anddresmx.cinefit.Adapters.TransactionsAdapter;
import com.anadromo.anddresmx.cinefit.Interfaces.SporaServices;
import com.anadromo.anddresmx.cinefit.Models.LoyaltyResponseError;
import com.anadromo.anddresmx.cinefit.Models.TransactionRequest;
import com.anadromo.anddresmx.cinefit.Models.UserResponse;
import com.anadromo.anddresmx.cinefit.Models.UserTransactionResponse;
import com.anadromo.anddresmx.cinefit.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andresrodriguez on 25/03/18.
 */

public class BalanceActivity extends AppCompatActivity{

    public static final String USER_LOGGED = "USER";

    TextView tvName;
    TextView tvEmail;
    TextView tvLoyaltyLevel;
    EditText etCardNumber;
    RecyclerView rvTransactionsRecycler;
    TransactionsAdapter adapter;
    UserResponse userResponse;
    Gson gson;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);

        if(getIntent().getExtras() == null){
            goToMainActivity();
            finish();
        }

        gson = new GsonBuilder().create();
        String user = getIntent().getStringExtra(BalanceActivity.USER_LOGGED);
        userResponse = new GsonBuilder().create().fromJson(user, UserResponse.class);

        initControls();
        setListeners();



    }

    private void initControls() {
        tvName = (TextView) findViewById(R.id.tvName);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvLoyaltyLevel = (TextView) findViewById(R.id.tvLoyaltyLevel);
        etCardNumber = (EditText) findViewById(R.id.etCinepolisCard);
        rvTransactionsRecycler = (RecyclerView) findViewById(R.id.rvTransactionsRecycler);
        rvTransactionsRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        rvTransactionsRecycler.hasFixedSize();
    }

    private void setListeners(){
        etCardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                String cardNumber = s.toString();
                if(cardNumber.length() == 16){
                    callRetrofitUserTransactionBalance(cardNumber);
                }
            }
        });
    }

    private void callRetrofitUserTransactionBalance(String cardNumber){
        String ENDPOINT = getString(R.string.endpoint_cinepolis_stage);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();

        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setCardNumber(cardNumber);
        transactionRequest.setCountryCode(userResponse.getCountryCode());
        transactionRequest.setTransactionInclude(true);

        SporaServices sporaServices = retrofit.create(SporaServices.class);


        sporaServices.getMembersLoyalty(userResponse.getAccessToken(), transactionRequest).enqueue(new Callback<UserTransactionResponse>() {
            @Override
            public void onResponse(Call<UserTransactionResponse> call, Response<UserTransactionResponse> response) {
                if (response.isSuccessful()) {
                    UserTransactionResponse userTransactionResponse = response.body();
                    tvName.setText(getString(R.string.label_name) + " " + userTransactionResponse.getName());
                    tvEmail.setText(getString(R.string.label_email) + " " + userTransactionResponse.getEmail());
                    tvLoyaltyLevel.setText(getString(R.string.label_loyalty_level) + " " + userTransactionResponse.getLevel().getName());

                    updateAdapter(userTransactionResponse.getTransactions());

                } else {
                    LoyaltyResponseError loyaltyResponseError = null;
                    try {
                        loyaltyResponseError = gson.fromJson(response.errorBody().string(),LoyaltyResponseError.class);
                        Toast.makeText(getApplication(), loyaltyResponseError.getStatusCode() + " - "+ loyaltyResponseError.getMessage(), Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserTransactionResponse> call, Throwable t) {
                Log.e("", "onFailure");
            }
        });
    }


    public void updateAdapter(List<UserTransactionResponse.Transaction> userTransactionResponseList){
        adapter = new TransactionsAdapter(userTransactionResponseList);
        rvTransactionsRecycler.setAdapter(adapter);
        rvTransactionsRecycler.getAdapter().notifyDataSetChanged();
    }

    private void goToMainActivity(){
        Intent intent = new Intent(BalanceActivity.this, MainActivity.class );
        startActivity(intent);
    }

}