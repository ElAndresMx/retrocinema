package com.anadromo.anddresmx.cinefit.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.anadromo.anddresmx.cinefit.Interfaces.SporaServices;
import com.anadromo.anddresmx.cinefit.Models.ErrorResponse;
import com.anadromo.anddresmx.cinefit.Models.UserRequest;
import com.anadromo.anddresmx.cinefit.Models.UserResponse;
import com.anadromo.anddresmx.cinefit.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    EditText etUsername;
    EditText etPassword;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initControls();


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                if(fieldValidation(username, password)){
                    // Make Login Request
                    callRetrofitOAuthService(username, password);
                }
            }
        });

    }

    private void initControls() {
        btnLogin = (Button) findViewById(R.id.btnLogin);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etUsername = (EditText) findViewById(R.id.etUsername);
    }


    private Boolean fieldValidation(String username, String password){

        if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password)){
            Toast.makeText(MainActivity.this, "Campos Vacios", Toast.LENGTH_LONG).show();
            return false;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(username).matches()){
            etUsername.setError("La dirección de correo electrónico no es valida.");
            return false;
        }

        if(password.length() < 4){
            etPassword.setError("La contraseña debe ser de 4 o más caracteres.");
            return false;
        }

        return true;
    }

    private void callRetrofitOAuthService(String username, String password) {
        String ENDPOINT = getString(R.string.endpoint_cinepolis_stage);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();

        UserRequest userRequest = new UserRequest();
        userRequest.setUsername(username);
        userRequest.setPassword(password);
        userRequest.setClientId(getString(R.string.client_id));
        userRequest.setClientSecret(getString(R.string.cliente_secret));
        userRequest.setCountryCode(getString(R.string.country_code));
        userRequest.setGrantType(getString(R.string.grant_type));

        SporaServices sporaServices = retrofit.create(SporaServices.class);


        sporaServices.loginAttempt(
                userRequest.getCountryCode(),
                userRequest.getUsername(),
                userRequest.getPassword(),
                userRequest.getGrantType(),
                userRequest.getClientId(),
                userRequest.getClientSecret()
        ).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                Gson gson = new GsonBuilder().create();
                if (response.isSuccessful()) {
                    makeToast("Login Successful.\nWelcome " + response.body().getUsername());
                    UserResponse userResponse = response.body();
                    String user = gson.toJson(userResponse, UserResponse.class);
                    goUserBalanceActivity(user);
                } else {
                    ErrorResponse errorResponse = null;
                    try {
                        errorResponse = gson.fromJson(response.errorBody().string(),ErrorResponse.class);
                        makeToast( errorResponse.getError() + " - "+ errorResponse.getErrorDescription());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.e("", "onFailure");
            }
        });
    }

    private void makeToast(String msg){
        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    private void goUserBalanceActivity(String user){
        Intent intent = new Intent(this, BalanceActivity.class);
        intent.putExtra(BalanceActivity.USER_LOGGED, user);
        startActivity(intent);
    }
}
