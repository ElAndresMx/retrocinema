package com.anadromo.anddresmx.cinefit.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andresrodriguez on 25/03/18.
 */

public class ErrorResponse {
    private String error;
    @SerializedName("error_description")
    private String errorDescription;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
