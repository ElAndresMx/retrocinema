package com.anadromo.anddresmx.cinefit.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.anadromo.anddresmx.cinefit.Models.UserTransactionResponse;
import com.anadromo.anddresmx.cinefit.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by andresrodriguez on 25/03/18.
 */

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.ViewHolder> {

    private List<UserTransactionResponse.Transaction> transactionList;

    public TransactionsAdapter(List<UserTransactionResponse.Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCinema;
        TextView tvMessage;
        TextView tvDate;
        TextView tvPoints;

        public ViewHolder(View view) {
            super(view);
            tvCinema = (TextView) view.findViewById(R.id.tvCinema);
            tvMessage = (TextView) view.findViewById(R.id.tvMessage);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvPoints = (TextView) view.findViewById(R.id.tvPoints);
        }
    }

    @Override
    public TransactionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.balance_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TransactionsAdapter.ViewHolder holder, int position) {
        UserTransactionResponse.Transaction transaction = transactionList.get(position);
        holder.tvCinema.setText(holder.itemView.getContext().getString(R.string.label_cinema) + " " + transaction.getCinema());
        holder.tvMessage.setText(holder.itemView.getContext().getString(R.string.label_message) + " " + transaction.getMessage());
        holder.tvDate.setText(holder.itemView.getContext().getString(R.string.label_date) + " " + dateFormat(transaction.getDate()));
        holder.tvPoints.setText(holder.itemView.getContext().getString(R.string.label_points) + " " + String.valueOf(transaction.getPoints()));
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }

    private String dateFormat(String date){
        String formatedDate = "";
        try{
            SimpleDateFormat parse = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.ENGLISH);
            Date aux = parse.parse(date);
            formatedDate = formatter.format(aux);
        }catch (ParseException e) {
            e.printStackTrace();
        }
        return formatedDate;
    }
}
