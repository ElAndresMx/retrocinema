package com.anadromo.anddresmx.cinefit.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andresrodriguez on 25/03/18.
 */

public class LoyaltyResponseError {
    private String message;
    @SerializedName("status_code")
    private String statusCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
}
